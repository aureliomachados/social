@extends('app')

@section('title')
    {{$user->name}}
@endsection

@section('content')
    <div class="container">

        {{-- Error messages --}}
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Opa!</strong> Existe algum problema com os valores informados<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row">
            <div class="col-md-3">
                <center><img width="160" height="160" class="img-circle" src="{{(Auth::user()->avatar) ? Auth::user()->avatar : "images/default-profile-picture.png"}}" alt="Imagem do perfil de {{Auth::user()->name}}"></center>
                <hr/>
                <p class="text-center">{{$user->name}}</p>
                <br/>
                <form action="{{route('profile.destroy', ['id' => Auth::user()->id])}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <input type="hidden" name="_method" value="DELETE"/>

                    <center><button type="submit" class="btn btn-danger" disabled><i class="glyphicon glyphicon-remove"></i> Excluir conta</button></center>
                </form>
            </div>

            <div class="col-lg-9">

                <div class="row">

                    <!--form informations-->
                    <div class="col-md-12">

                        <div class="row">

                                <!-- tabs-->
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#nao-validadas"><i class="glyphicon glyphicon-info-sign"></i> Doações não validadas</a></li>
                                    <li><a data-toggle="tab" href="#validadas"><i class="glyphicon glyphicon-check"></i> Validadas</a></li>
                                </ul>


                                <div class="tab-content">
                                    <div id="nao-validadas" class="tab-pane fade in active">
                                        <br/>

                                        <table class="table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Nome do livro</th>
                                                    <th>Quantidade</th>
                                                    <th>Data</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($doacoes as $doacao)
                                                    @if($doacao->validar == false)
                                                        <tr>
                                                            <td><a href="{{route('doacoes.show', ['id' => $doacao->id])}}">{{$doacao->livro}}</a></td>
                                                            <td>{{$doacao->quantidade}}</td>
                                                            <td>{{date_format($doacao->created_at, 'd/m/Y')}}</td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                    <div id="validadas" class="tab-pane fade">
                                        <br/>

                                        <table class="table table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th>Nome do livro</th>
                                                <th>Quantidade</th>
                                                <th>Data</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($doacoes as $doacao)
                                                @if($doacao->validar == true)
                                                    <tr>
                                                        <td><a href="{{route('doacoes.show', ['id' => $doacao->id])}}">{{$doacao->livro}}</a></td>
                                                        <td>{{$doacao->quantidade}}</td>
                                                        <td>{{date_format($doacao->created_at, 'd/m/Y')}}</td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                        </div>

                    </div>
                </div>

                <hr/>

                <!--Posts informations -->
                <div class="col-md-12">



                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
