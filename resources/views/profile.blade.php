@extends('app')

@section('title')
    {{$user->name}}
@endsection

@section('content')
    <div class="container">

        {{-- Error messages --}}
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Opa!</strong> Existe algum problema com os valores informados<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row">
            <div class="col-md-3">
                <center><img width="160" height="160" class="img-circle" src="{{(Auth::user()->avatar) ? Auth::user()->avatar : "images/default-profile-picture.png"}}" alt="Imagem do perfil de {{Auth::user()->name}}"></center>
                <hr/>
                <p class="text-center">{{$user->name}}</p>
                <br/>
                <form action="{{route('profile.destroy', ['id' => Auth::user()->id])}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <input type="hidden" name="_method" value="DELETE"/>

                    <center><button type="submit" class="btn btn-danger" disabled><i class="glyphicon glyphicon-remove"></i> Excluir conta</button></center>
                </form>
            </div>

            <div class="col-lg-9">

                <div class="row">

                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#minhas-doacoes">Minhas doações</span> </a></li>
                        <li><a data-toggle="tab" href="#minhas-postagens">Minhas postagens</a></li>
                    </ul>



                    <div class="tab-content">


                        <div id="minhas-doacoes" class="tab-pane fade in active">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Livro</th>
                                        <th>Quantidade</th>
                                        <th>Entidade</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($user->doacoes as $doacao)
                                        <tr>
                                            <td>{{$doacao->livro}}</td>
                                            <td>{{$doacao->quantidade}}</td>
                                            <td>{{$doacao->entidade->name}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>


                        <div id="minhas-postagens" class="tab-pane fade">


                            <!--form informations-->
                            <div class="col-md-12">

                                <div class="page-header">
                                    <h3>Leu um livro recentemente?</h3>
                                </div>


                                <div class="row">

                                    <div class="col-md-10 col-md-offset-1">

                                        <form action="{{route('posts.store')}}" method="post" class="form-horizontal">

                                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}"/>

                                            <div class="form-group">
                                                <input type="text" name="title" id="title" value="{{old('title')}}" placeholder="Nome do livro" class="form-control" required="true"/>
                                            </div>

                                            <div class="form-group">
                                                <textarea name="body" id="body" class="form-control" placeholder="Compartilhe aqui conosco a sua experiência." rows="10" required="true">{{old('body')}}</textarea>
                                            </div>

                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="glyphicon glyphicon-send"></i> Publicar
                                                </button>
                                            </div>

                                        </form>

                                    </div>

                                </div>

                                <hr/>

                                <!--Posts informations -->
                                <div class="col-md-12">

                                    @foreach($user->posts->sortByDesc('created_at') as $post)

                                        <div class="panel panel-success">

                                            <div class="panel-heading panel-title text-right">{{date('d/m/Y à\s H:i', strtotime($post->created_at))}}
                                                <form action="{{route('posts.destroy', ['id' => $post->id])}}" method="post">
                                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                                    <input type="hidden" name="_method" value="DELETE"/>
                                                    <button type="submit" class="btn btn-danger">Excluir</button>
                                                </form>
                                            </div>

                                            <div class="panel-body">
                                                <div class="page-header">
                                                    <h4><strong>{{$post->title}}</strong></h4>
                                                </div>

                                                <p>
                                                    {{$post->body}}
                                                </p>
                                            </div>
                                        </div>

                                    @endforeach

                                </div>

                            </div>
                        </div>

                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
@endsection
