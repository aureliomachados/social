@extends('app')


@section('title')
    {{$user->name}}
@endsection

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-3">

                <center><img width="160" height="160" class="img-circle" src="/{{($user->avatar) ? $user->avatar : "images/default-profile-picture.png"}}" alt="Imagem do perfil de {{$user->name}}"></center>
                <hr/>
                <p class="text-center">{{$user->name}}</p>

                <br/><br/>
                <!--generos favoritos do perfil -->
                <div class="panel panel-success">

                    <div class="panel-heading panel-title text-center">Gêneros favoritos de <strong>{{$user->name}}</strong></div>

                    <div class="panel-body">
                        <ul class="list-group">
                            @if(isset($user))
                                @foreach($user->generolivros as $genero)
                                    <li class="list-group-item">{{$genero->nome}}</li>
                                @endforeach
                            @endif
                        </ul>
                    </div>

                </div>
            </div>

            <div class="col-lg-9">

                <div class="panel panel-default">
                    <div class="panel-heading panel-title text-center">Últimas postagens de {{$user->name}}</div>

                    <div class="panel-body">

                       @if($user->posts)
                        @foreach($user->posts->sortByDesc('created_at') as $post)

                            <div class="panel panel-success">

                                <div class="panel-heading panel-title text-right">{{date('d/m/Y à\s H:i', strtotime($post->created_at))}}</div>

                                <div class="panel-body">
                                    <div class="page-header">
                                        <h4><strong>{{$post->title}}</strong></h4>
                                    </div>

                                    <p>
                                        {{$post->body}}
                                    </p>
                                </div>
                            </div>

                        @endforeach

                       @endif

                    </div>
                </div>

            </div>
        </div>

    </div>

@endsection