@extends('app')

@section('title')
    Lista de gêneros
@endsection

@section('content')
    <div class="container">

        <div class="page-header">
            <h4>Adicionar gêneros aos favoritos</h4>
        </div>

        <div class="row">

            <div class="col-md-4 col-md-offset-4">

                <div class="panel panel-primary">

                    <div class="panel-heading panel-title text-center">Lista de generos</div>

                    <div class="panel-body">

                        <ul class="list-group">
                           @if($generos)
                            @foreach($generos as $genero)
                                <li class="list-group-item">
                                    <form action="{{route('generosfavoritos.save')}}" method="post" class="form-inline">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <input type="hidden" name="genero_id" value="{{$genero->id}}"/>

                                            {{$genero->nome}}

                                            <button type="submit" class="btn btn-success">
                                                <i class="glyphicon glyphicon-plus"></i>
                                            </button>
                                    </form>
                                </li>
                            @endforeach

                               @else
                                   <span class="text-danger"> Não há mais gêneros disponíveis.</span>

                            @endif
                        </ul>

                    </div>
                </div>

            </div>

        </div>

    </div>
@endsection