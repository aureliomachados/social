Olá <strong>{{$usuario->name}}</strong>.<br/>

A entidade <strong>{{$entidade->name}}</strong> acaba de validar a sua doação.<br/>

E deixou a seguinte mensagem à você:<br/>
<pre>
    {{$agradecimento}}
</pre>