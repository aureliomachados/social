
@extends('app')

@section('title')
    Lista de entidades
@endsection

@section('content')
    <div class="container">
        <div class="row">


            <div class="col-md-3">

                {{-- inclui os gênerosf --}}

            </div>

            <div class="col-md-9">

                <div class="panel panel-default">

                    <div class="panel-heading panel-title">Entidades</div>

                    <div class="panel-body">

                        <div class="row">
                            @foreach($entidades as $entidade)
                                <div class="col-sm-6 col-md-4">

                                    <div class="thumbnail hoverable">
                                        <a href="{{route('entidades.details', ['id' => $entidade->id])}}"><img class="img-circle" src="{{($entidade->avatar) ? '/' . $entidade->avatar : "/images/default-profile-picture.png"}}" alt="{{$entidade->name}}" height="100" width="100"></a>
                                        <div class="caption">
                                            <div class="page-header">
                                                <h3>{{$entidade->name}} {{($entidade->id == Auth::user()->id) ? "(Eu)" : ""}}</h3>
                                            </div>

                                            @if(!(Auth::user()->friends->contains($entidade->id)) && Auth::user()->id != $entidade->id)
                                                <form action="{{route('seguir')}}" method="post">
                                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                                    <input type="hidden" name="user_id" value="{{$entidade->id}}"/>

                                                    <button type="submit" class="btn btn-danger btn-fab btn-raised mdi-action-grade" data-toggle="tooltip" data-placement="bottom" title="Adicionar aos favoritos">
                                                        Seguir
                                                    </button>
                                                </form>
                                            @endif

                                            @if((Auth::user()->id != $entidade->id) && (Auth::user()->friends->contains($entidade->id)))
                                                <form action="{{route('deixarDeSeguir')}}" method="post">
                                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                                    <input type="hidden" name="user_id" value="{{$entidade->id}}"/>

                                                    <button type="submit" class="btn btn-success btn-fab btn-raised mdi-action-grade" data-toggle="tooltip" data-placement="bottom" title="Remover dos favoritos">
                                                        Deixar de seguir
                                                    </button>
                                                </form>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                    </div>

                    <div class="panel-footer text-center">
                        @unless($paginate == false)
                            {!! $entidades->render() !!}
                        @endunless
                    </div>
                </div>
                <span class="spinner-blue">{{$entidade->count()}}</span> registro(s) encontrado(s) nesta pagina.
            </div>
        </div>
    </div>
@endsection
