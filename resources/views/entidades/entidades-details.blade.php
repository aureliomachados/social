@extends('app')


@section('title')
    {{$entidade->name}}
@endsection

@section('content')

    <div class="container" xmlns="http://www.w3.org/1999/html">

        <div class="row">
            <div class="col-md-3">

                <center><img width="160" height="160" class="img-circle" src="/{{($entidade->avatar) ? $entidade->avatar : "images/default-profile-picture.png"}}" alt="Imagem do perfil de {{$entidade->name}}"></center>
                <hr/>
                <p class="text-center">{{$entidade->name}}</p>
            </div>

            <div class="col-lg-9">

                <div class="page-header">
                    <h5><strong>{{$entidade->name}}</strong></h5>
                </div>

                <!-- tabs-->
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#informacoes-basicas"><i class="glyphicon glyphicon-info-sign"></i> Informações básicas</a></li>
                    <li><a data-toggle="tab" href="#contato"><i class="glyphicon glyphicon-phone"></i> Contato</a></li>
                    <li><a data-toggle="tab" href="#doar"><i class="glyphicon glyphicon-gift"></i> Fazer doação</a></li>
                </ul>


                <div class="tab-content">
                    <div id="informacoes-basicas" class="tab-pane fade in active">
                        <br/>

                        <strong>Nome:</strong>
                        <p>{{$entidade->name}}</p>

                        <strong>Quem somos?</strong>
                        <p>{{$entidade->detalhe->quemsomos}}</p>

                        <strong>Missão:</strong>
                        <p>{{$entidade->detalhe->missao}}</p>

                        <strong>Visão:</strong>
                        <p>{{$entidade->detalhe->visao}}</p>

                        <strong>Objetivos:</strong>
                        <p>{{$entidade->detalhe->objetivos}}</p>
                    </div>


                    <div id="contato" class="tab-pane fade">
                        <br/>

                        <strong>E-mail:</strong>
                        <p>{{$entidade->email}}</p>

                        <strong>Telefone:</strong>
                        <p>{{$entidade->detalhe->telefone}}</p>

                        <strong>Endereço:</strong>
                        <p>{{$entidade->detalhe->endereco}}</p>
                    </div>

                    <div id="doar" class="tab-pane fade">
                        <br/>

                        @include('entidades.partials.formulario-doacao')

                    </div>
                    <!--endtabs-->

            </div>
        </div>

    </div>

@endsection