@extends('app')

@section('title')
    Registrar
@endsection

@section('content')
    <div class="container-fluid" xmlns="http://www.w3.org/1999/html">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading panel-title text-center">Cadastrar nova entidade</div>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Opa!</strong> Existe algum problema com os valores informados<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" action="{{route('entidades.store')}}">

                            <fieldset>
                                <legend>Informações básicas </legend>
                                <p class="text-right"><small>Os campos com <span style="color: red">(*)</span> são obrigatórios</small></p>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Quem somos</label>
                                    <div class="col-md-6">
                                        <textarea class="form-control" name="quemsomos" id="quemsomos">{{old('quemsomos')}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Missão</label>
                                    <div class="col-md-6">
                                        <textarea class="form-control" name="missao" id="missao">{{old('missao')}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Visão</label>
                                    <div class="col-md-6">
                                        <textarea class="form-control" name="visao" id="visao">{{old('visao')}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Objetivos</label>
                                    <div class="col-md-6">
                                        <textarea class="form-control" name="objetivos" id="objetivos">{{old('objetivos')}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Telefone</label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="tel" name="telefone" id="telefone" value="{{old('telefone')}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Endereço</label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="tel" name="endereco" id="endereco" value="{{old('endereco')}}">
                                    </div>
                                </div>

                            </fieldset>


                            <fieldset>

                                <legend>Informações de acesso</legend>

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Nome *</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="name" value="{{ old('name') }}" required="true">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">E-mail *</label>
                                    <div class="col-md-6">
                                        <input type="email" class="form-control" name="email" value="{{ old('email') }}" required="true">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Senha *</label>
                                    <div class="col-md-6">
                                        <input type="password" class="form-control" name="password" required="true">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Confirmar senha *</label>
                                    <div class="col-md-6">
                                        <input type="password" class="form-control" name="password_confirmation" required="true">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-success">
                                            Cadastrar
                                        </button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
