<form class="form-horizontal" action="{{route('doacoes.send')}}" method="post">
    <input type="hidden" name="_token" value="{{csrf_token()}}"/>

    <input type="hidden" name="user_id" value="{{Auth()->user()->id}}"/>
    <input type="hidden" name="entidade_id" value="{{$entidade->id}}">

    <fieldset>

        <!-- Form Name -->
        <legend>Formalizar doação</legend>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="livro">Livro</label>
            <div class="col-md-4">
                <input id="livro" name="livro" type="text" placeholder="Digite o nome do livro" class="form-control input-md" required="">
                <span class="help-block">Exemplo: A mão e a Luva</span>
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="quantidade">Quantidade</label>
            <div class="col-md-4">
                <input id="quantidade" name="quantidade" type="number" placeholder="" class="form-control input-md" min="1">

            </div>
        </div>

        <!-- Textarea -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="mensagem">Mensagem</label>
            <div class="col-md-4">
                <textarea class="form-control" id="mensagem" name="mensagem" required=""></textarea>
            </div>
        </div>

        <!-- Button (Double) -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="enviar"></label>
            <div class="col-md-8">
                <button type="submit" id="enviar" name="enviar" class="btn btn-success"><i class="glyphicon glyphicon-send"></i> Enviar</button>
                <button type="reset" id="cancelar" name="cancelar" class="btn btn-danger">Cancelar</button>
            </div>
        </div>

    </fieldset>
</form>
