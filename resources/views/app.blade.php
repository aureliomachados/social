<!DOCTYPE html>
<html lang="en">
<head>
    <!--<meta http-equiv="refresh" content="5">-->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="Aurélio Guedes">
    <meta name="keywords" content="Doações,Livros,Leia,Leitura,Literatura">
    <meta name="description" content="Um lugar para amantes da leitura">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Doe Livros - @yield('title')</title>

    <link rel="shortcut icon" type="image/png" href="/images/favicon-16.png"/>


	<link href="/css/app.css" rel="stylesheet">
    <!--<link href="/bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet">-->

    <!--Bootstrap default -->
    <link href="/bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet">

    <!-- Material Design for Bootstrap -->
    <link href="/bower_components/bootstrap-material-design/dist/css/roboto.min.css" rel="stylesheet">
    <link href="/bower_components/bootstrap-material-design/dist/css/material-fullpalette.min.css" rel="stylesheet">
    <link href="/bower_components/bootstrap-material-design/dist/css/ripples.min.css" rel="stylesheet">

    <!--social buttons -->
    <link href="/css/font-awesome.css" rel="stylesheet">
    <link href="/css/bootstrap-social.css" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

    <!-- Jquery -->
    <script src="/bower_components/jquery/dist/jquery.js"></script>
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				  <a class="navbar-brand" href="{{Auth::guest() ? route('welcome') : route('home')}}"><i class="glyphicon glyphicon-book"></i> Doe Livros</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">

                    {{-- no lugar de false ficará o role do usuário como administrador --}}
                    @unless(Auth::guest() || !(Auth::user()->perfil == "administrador"))
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Cadastro<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{route('entidades.create')}}"><i class="glyphicon glyphicon-plus"></i> Entidade</a></li>
                            <li><a href="{{route('generolivro.index')}}"><i class="glyphicon glyphicon-list"></i> Gênero de livros</a></li>
                            <li><a href="{{route('contatos')}}"><i class="glyphicon glyphicon-list"></i> Contatos</a></li>
                        </ul>
                    </li>
                    @endunless
                    @unless(Auth::guest())
                        <li><a href="{{route('entidades')}}">Entidades</a></li>
                    @endunless
				</ul>

				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
						<li><a href="/auth/login"><i class="glyphicon glyphicon-log-in"></i> Entrar</a></li>
						<li><a href="/auth/register"><i class="glyphicon glyphicon-plus"></i> Cadastrar</a></li>
					@else
                        <li>
                            @if(Auth::user()->perfil == "entidade")
                            <a href="{{route('profile.entity')}}"><img width="25" height="25" class="img-rounded" src="{{(Auth::user()->avatar) ? "/" . Auth::user()->avatar : "/images/default-profile-picture.png"}}" alt="Imagem do perfil de {{Auth::user()->name}}"/></a>
                                @else
                                <a href="{{route('profile')}}"><img width="25" height="25" class="img-rounded" src="{{(Auth::user()->avatar) ? "/" . Auth::user()->avatar : "/images/default-profile-picture.png"}}" alt="Imagem do perfil de {{Auth::user()->name}}"/></a>
                                @endif
                        </li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="glyphicon glyphicon-option-vertical"></span></a>
							<ul class="dropdown-menu" role="menu">
                                @if(Auth::user()->perfil == "entidade")
                                    <li><a href="{{route('profile.entity')}}"><i class="glyphicon glyphicon-user"></i> Meu perfil</a> </li>
                                    @else
                                    <li><a href="{{route('profile')}}"><i class="glyphicon glyphicon-user"></i> Meu perfil</a> </li>
                                @endif
                                <li><a href="{{route('avatar.create')}}"><i class="glyphicon glyphicon-picture"></i> Editar avatar</a> </li>
								<li><a href="/auth/logout"><i class="glyphicon glyphicon-log-out"></i> Sair</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>

    <div class="container">
        @include('flash::message')
    </div>

	@yield('content')


	<!--<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>-->
    <script src="/md_bootstrap/js/bootstrap.min.js"></script>
    <!--<script src="/md_bootstrap/mdb.js"></script>-->
    <!-- Material Design for Bootstrap -->
    <script src="/bower_components/bootstrap-material-design/dist/js/material.min.js"></script>
    <script src="/bower_components/bootstrap-material-design/dist/js/ripples.min.js"></script>
    <script>
        $.material.init();
    </script>
</body>
</html>
