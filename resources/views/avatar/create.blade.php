@extends('app')

@section('title', 'Adicionar imagem ao perfil')

@section('content')
    <div class="container">

        <div class="page-header">

            <p><strong><i class="glyphicon glyphicon-alert"></i> Por favor</strong>, para prosseguir com o uso do aplicativo, adicione uma imagem ao perfil.</p>

        </div>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <form class="form-horizontal" action="{{route('avatar.send')}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <fieldset>

                        <!-- Form Name -->
                        <legend>Imagem do perfil</legend>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="avatar">Imagem</label>
                            <div class="col-md-4">
                                <input id="avatar" name="avatar" type="file" placeholder="" class="form-control input-md" required="true">

                            </div>
                        </div>

                        <!-- Button -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="avatar"></label>
                            <div class="col-md-4">
                                <button type="submit" id="avatar" name="avatar" class="btn btn-success">Carregar</button>
                            </div>
                        </div>

                    </fieldset>
                </form>

            </div>
        </div>
    </div>
@endsection