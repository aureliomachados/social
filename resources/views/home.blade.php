
@extends('app')

@section('title')
    Lista de perfis
@endsection

@section('content')
<div class="container">
	<div class="row">


        <div class="col-md-3">

            {{-- inclui os gênerosf --}}
            @include('partials.generos-favoritos')

        </div>

		<div class="col-md-9">

            <form action="{{route('profile.busca')}}" method="get" class="form-inline">
                <div class="form-group">
                    <input type="search" placeholder="Nome" name="name" id="name" class="form-control" required="" onfocus="" autofocus=""/>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                        <i class="glyphicon glyphicon-search"></i> Buscar
                    </button>
                </div>
            </form>

            <hr/>

            <br/>

			<div class="panel panel-default">

				<div class="panel-heading">Perfis</div>

                <div class="panel-body">

                    <div class="row">
                                @foreach($users as $user)
                                 <div class="col-sm-6 col-md-4">

                                    <div class="thumbnail hoverable">
                                        @if($user->perfil == "entidade")
                                            <a href="{{route('entidades.details', ['id' => $user->id])}}"><img class="img-circle" src="{{($user->avatar) ? '/' . $user->avatar : "/images/default-profile-picture.png"}}" alt="{{$user->name}}" height="100" width="100"></a>
                                            @else
                                            <a href="{{route('profile.details', ['id' => $user->id])}}"><img class="img-circle" src="{{($user->avatar) ? '/' . $user->avatar : "/images/default-profile-picture.png"}}" alt="{{$user->name}}" height="100" width="100"></a>
                                        @endif
                                        <div class="caption">
                                           <div class="page-header">
                                               <p>{{$user->name}} {{($user->id == Auth::user()->id) ? "(Eu)" : ""}}</p>
                                           </div>

                                            @if(!(Auth::user()->friends->contains($user->id)) && Auth::user()->id != $user->id)
                                                <form action="{{route('seguir')}}" method="post">
                                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                                    <input type="hidden" name="user_id" value="{{$user->id}}"/>

                                                    <button type="submit" class="btn btn-danger btn-fab btn-raised mdi-action-grade" data-toggle="tooltip" data-placement="bottom" title="Adicionar aos favoritos">
                                                        Seguir
                                                    </button>
                                                </form>
                                            @endif

                                            @if((Auth::user()->id != $user->id) && (Auth::user()->friends->contains($user->id)))
                                                <form action="{{route('deixarDeSeguir')}}" method="post">
                                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                                    <input type="hidden" name="user_id" value="{{$user->id}}"/>

                                                    <button type="submit" class="btn btn-success btn-fab btn-raised mdi-action-grade" data-toggle="tooltip" data-placement="bottom" title="Remover dos favoritos">
                                                        Deixar de seguir
                                                    </button>
                                                </form>
                                            @endif
                                        </div>
                                    </div>
                                 </div>
                                @endforeach
                    </div>

                </div>

                <div class="panel-footer text-center">
                    @unless($paginate == false)
                        {!! $users->render() !!}
                    @endunless
                </div>
			</div>
            <span class="spinner-blue">{{$users->count()}}</span> registro(s) encontrado(s) nesta pagina.
        </div>
	</div>
</div>
@endsection
