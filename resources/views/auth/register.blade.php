@extends('app')

@section('title')
    Registrar
@endsection

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading panel-title text-center">Cadastrar</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
                            <strong>Opa!</strong> Existe algum problema com os valores informados<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="/auth/register">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Nome</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="name" value="{{ old('name') }}" required="true">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}" required="true">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Senha</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password" required="true">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Confirmar senha</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation" required="true">
							</div>
						</div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Captcha</label>
                            <div class="col-md-6">
                                {!! Recaptcha::render() !!}
                            </div>
                        </div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-success">
									Cadastrar
								</button>
							</div>
						</div>

                        <!--
                        <div class="col-md-6 col-md-offset-4">
                            <p>Ou você pode: </p>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a href="/login/facebook" class="btn btn-block btn-social btn-facebook">
                                    <i class="fa fa-facebook"></i> Entrar com Facebook
                                </a>
                            </div>
                        </div>
                        -->
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
