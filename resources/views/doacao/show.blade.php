@extends('app')

@section('title')
    {{$user->name}}
@endsection

@section('content')
    <div class="container">

        {{-- Error messages --}}
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Opa!</strong> Existe algum problema com os valores informados<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row">
            <div class="col-md-3">
                <center><img width="160" height="160" class="img-circle" src="{{(Auth::user()->avatar) ? '/' . Auth::user()->avatar : "images/default-profile-picture.png"}}" alt="Imagem do perfil de {{Auth::user()->name}}"></center>
                <hr/>
                <p class="text-center">{{$user->name}}</p>
                <br/>
                <form action="{{route('profile.destroy', ['id' => Auth::user()->id])}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <input type="hidden" name="_method" value="DELETE"/>
                </form>
            </div>

            <div class="col-lg-9">

                <div class="row">

                    <!--form informations-->
                    <div class="col-md-12">

                        <div class="row">

                            <!-- tabs-->
                            <div class="panel panel-primary">
                                <div class="panel-heading panel-title text-center">
                                    Doacão feita por <a href="{{route('profile.details', ['id' => $doador->id])}}">{{$doador->name}}</a>
                                </div>

                                <div class="panel-body">
                                    <br/>
                                    <span class="pull-right"><strong>Data: </strong>{{date_format($doacao->created_at, 'd/m/Y')}}</span>
                                    <h1><strong>Livro: </strong>{{$doacao->livro}}</h1>
                                    <p><strong>Quantidade: </strong> {{$doacao->quantidade}}</p>
                                    <strong>Mensagem: </strong>
                                    <p>{{$doacao->mensagem}}</p>
                                    <br/>

                                    @if(Auth::user()->id == $doacao->entidade_id && $doacao->validar == false)
                                        <form action="{{route('doacoes.validar')}}" method="post" class="form-horizontal">
                                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                            <input type="hidden" name="_method" value="PUT"/>
                                            <input type="hidden" name="id" value="{{$doacao->id}}"/>

                                            <!-- Textarea -->
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    Agradecimentos: <textarea class="form-control" id="agradecimento" name="agradecimento" required=""></textarea>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-send"></i> Validar doação</button>

                                        </form>
                                    @endif
                                    <hr/>
                                    <a href="{{route('profile.entity')}}" class="btn btn-success"><i class="glyphicon glyphicon-backward"></i> Voltar</a>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
