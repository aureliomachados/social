@extends('app')

@section('title')
    Lista de mensagens enviadas
@endsection

@section('content')
    <div class="container">

        <div class="page-header">
            <h4>Lista de mensagens enviadas</h4>
        </div>

        <div class="row">

            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#nao-lidas">Mensagens não lidas <span class="badge red">{{$mensagensNaoLidas}}</span> </a></li>
                <li><a data-toggle="tab" href="#lidas">Mensagens lidas <span class="badge green">{{$contatos->count() - $mensagensNaoLidas}}</span> </a></li>
                <li><a data-toggle="tab" href="#todas">Todas as mensagens <span class="badge blue">{{$contatos->count()}}</span> </a></li>
            </ul>


            <div class="tab-content">

                <!--Não lidas-->
                <div id="nao-lidas" class="tab-pane fade in active">

                    @include('contato.partials.nao-lidas')

                </div>

                <!-- Mensagens lidas -->
                <div id="lidas" class="tab-pane fade">

                    @include('contato.partials.lidas')

                </div>


                <!--Todas-->
                <div id="todas" class="tab-pane fade">

                    @include('contato.partials.todas')

                </div>
            </div>

        </div>

    </div>
@endsection