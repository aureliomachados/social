@extends('app')

@section('title')
    Mensagem de {{$contato->nome}}
@endsection

@section('content')
    <div class="container">

        <div class="page-header">

            <h4>Mensagem enviada por <strong>{{$contato->nome}}</strong></h4>
        </div>
        <div class="row">
            <p class="well">{{$contato->mensagem}}</p>
            <span>No dia {{date('d/m/Y', strtotime($contato->created_at))}}</span><br/>
            <span>Email: <a href="mailto:{{$contato->email}}">{{$contato->email}}</a> </span><br/>
            <span>Telefone: {{$contato->telefone}}</span>
            <br/>

            <a class="btn btn-primary" href="{{route('contatos')}}"><i class="glyphicon glyphicon-backward"></i> Voltar</a>
        </div>
    </div>
@endsection