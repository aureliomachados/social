<!--Mensagens não lidas -->
<br/>
<div class="panel panel-primary">

    <div class="panel-heading panel-title text-center">Mensagens</div>

    <div class="panel-body">

        <table class="table table-bordered table-striped table-hover">

            <thead>
            <tr>
                <th>Nome</th>
                <th>Email</th>
                <th>Ver</th>
            </tr>
            </thead>
            <tbody>
            @if($contatos)
                @foreach($contatos as $contato)
                    @unless($contato->visualizado)
                        <tr>
                            <td>{{$contato->nome}}</td>
                            <td>{{$contato->email}}</td>
                            <td><a href="{{route('contato.ver', ['id' => $contato->id])}}">ver</a> </td>
                        </tr>
                    @endunless

                @endforeach
            @else
                <span class="text-danger"> Não há mais mensagens.</span>
            @endif
            </tbody>

        </table>
    </div>
</div>
