@extends('app')

@section('title')
    Formulário de contato
@endsection


@section('content')
    <div class="container">

        <div class="page-header">

            <h5>Enviar mensagem de contato</h5>

        </div>

        <div class="row">

            <div class="col-lg-8 col-lg-offset-2">
                @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Opa!</strong> Existe algum problema com os valores informados<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" action="{{route('contato.envia')}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label class="col-md-4 control-label">Nome</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="nome" value="{{ old('nome') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">E-Mail</label>
                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Telefone</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="telefone" value="{{old('telefone')}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Mensagem</label>
                                <div class="col-md-6">
                                    <textarea class="form-control" name="mensagem">{{old('mensagem')}}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-success">
                                        <i class="glyphicon glyphicon-send"></i> Enviar mensagem
                                    </button>
                                </div>
                            </div>
                        </form>
            </div>
    </div>
@endsection
