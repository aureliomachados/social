<div class="panel panel-success">

    <div class="panel-heading panel-title text-center">Usuários</div>

    <div class="panel-body">

        <ul class="collection">
            @if(isset($generos))
                @foreach($generos as $genero)
                    <li class="collection-item">{{$genero->nome}}</li>
                @endforeach
            @endif
        </ul>

    </div>

    <div class="panel-footer">
        <a href="{{route('generosfavoritos.generos')}}" class="btn btn-primary btn-raised">Adicionar gêneros</a>
    </div>

</div>