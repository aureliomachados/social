@extends('app')

@section('title')
 Seja bem vindo
@endsection

@section('content')
    <div class="container">
        <div class="content">

            <div class="jumbotron">

                <h1 class="text-center"><i class="glyphicon glyphicon-book" style="color: green"></i> <b>Doe Livros</b></h1>
                <br/>
                <h3 class="text-center">Ler um bom livro... algo tão poderoso capaz de mudar o rumo da sociedade. Leia.</h3>
            </div>

        </div>

        <div class="panel panel-default">

            <div class="panel-heading panel-title text-center">Últimas doações realizadas pelos membros do aplicativo</div>

            <div class="panel-body">
                @foreach($doacoes as $doacao)
                    <div class="media">
                        <div class="media-left media-middle">
                            <figure>
                                <img class="img-rounded" src="{{$doacao->user->avatar}}" alt="Foto do perfil do usuário {{$doacao->user->name}}" width="150" height="150">
                                <figcaption class="text-center"><b>{{$doacao->user->name}}</b></figcaption>
                            </figure>
                        </div>
                        <div class="media-body">
                            <h4><b>Livro:</b> {{$doacao->livro}} <i>realizada para a entidade</i> {{$doacao->entidade->name}}</h4>
                            Mensagem deixada por <strong>{{$doacao->user->name}}</strong>
                            <p>{{$doacao->mensagem}}</p>
                        </div>
                    </div>
                    <hr/>
                @endforeach
            </div>
        </div>

        <div class="row">

            <div class="col-sm-4">
                <div class="page-header">
                    <h4 class="text-center"><b>Sonhar</b></h4>
                </div>

                <span>
                    <p><i>A Mão e a Luva (Machado de Assis)</i></p>
                    Eu creio que a senhora sonha talvez demais.
                    Sonhará uns amores de romance, quase impossíveis?
                    digo-lhe que faz mal, que é melhor, muito melhor contentar-se com a realidade;
                    se ela não é brilhante como os sonhos, tem pelo menos a vantagem de existir.
                </span>
            </div>

            <div class="col-sm-4">
                <div class="page-header">
                    <h4 class="text-center"><b>Literatura</b></h4>
                </div>

                <span>
                    <p><i>São Bernardo (Graciliano Ramos)</i></p>
                    Foi assim que sempre se fez. A literatura é a literatura, seu Paulo.
                    A gente discute, briga, trata de negócios naturalmente,
                    mas arranjar palavras com tinta é outra coisa.
                    Se eu fosse escrever como falo, ninguém me lia.
                </span>
            </div>

            <div class="col-sm-4">
                <div class="page-header">
                    <h4 class="text-center"><b>Sentimentos</b></h4>
                </div>
                <span>
                    <p><i>Érico Veríssimo</i></p>
                    Em geral quando termino um livro encontro-me numa confusão de sentimentos,
                    um misto de alegria, alívio e vaga tristeza.
                    Relendo a obra mais tarde, quase sempre penso:
                    Não era bem isto o que queria dizer.
                </span>
            </div>

        </div>


    </div>

    <footer>
        <div class="container">
            <div class="row">

                <div id="quem-somos" class="col-md-4">

                    <div class="page-header">
                        <h3>Quem somos?</h3>
                    </div>


                </div>

                <div id="links-uteis" class="col-md-4">

                    <div class="page-header">
                        <h3>Links úteis</h3>
                    </div>

                </div>

                <div id="doe-livros" class="col-md-4">

                    <div class="page-header">
                        <h3>Contato</h3>
                    </div>

                    <a href="{{route('contato')}}"><i class="glyphicon glyphicon-send"></i> Enviar mensagem de contato</a>

                </div>

            </div>
        </div>
    </footer>
@endsection
