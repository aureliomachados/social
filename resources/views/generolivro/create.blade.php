@extends('app')

@section('title')
    Adicionar novo gênero
@endsection

@section('content')
    <div class="container">

        <div class="page-header">
            <h1>Adicionar novo gênero</h1>
        </div>

        <div class="row">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="col-lg-6 col-lg-offset-3">
                <form action="{{route('generolivro.store')}}" class="form-horizontal" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                    <div class="form-group">
                        <label for="nome">Nome</label>
                        <input type="text" class="form-control" id="nome" name="nome" value="{{old('nome')}}" placeholder="Gênero" required="true">

                        <br/>
                        <input type="submit" value="Adicionar" class="btn btn-primary"/>
                        <input type="reset" value="Limpar" class="btn btn-warning"/>
                        <a href="{{route('generolivro.index')}}" class="btn btn-danger">Cancelar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection