@extends('app')

@section('title')
    Lista de gêneros
@endsection


@section('content')
    <div class="container">

        <div class="page-header">
            <h1>Lista de gêneros</h1>
        </div>

        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">

                <span class="left"><a href="{{route('generolivro.create')}}" class="btn btn-success">Adicionar novo</a></span>

                <br/>
                <br/>

                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Editar</th>
                            <th>Remover</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($generos as $genero)
                            <tr>
                                <td>{{$genero->nome}}</td>
                                <td><a href="{{route('generolivro.edit', ['id' => $genero->id])}}">Editar</a></td>
                                <td>
                                    <form action="{{route('generolivro.destroy', ['id' => $genero->id])}}" method="post">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                        <input type="hidden" name="_method" value="DELETE"/>
                                        <input type="submit" value="Remover" class="btn btn-danger"/>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {!!$generos->render()!!}
            </div>
        </div>
    </div>
@endsection