@extends('app')

@section('title')

@endsection

@section('content')
    <div class="container">

        <div class="page-header">
            <h1>Editando gênero</h1>
        </div>

        <div class="row">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="col-lg-6 col-lg-offset-3">
                <form action="{{route('generolivro.update', ['id' => $genero->id])}}" class="form-horizontal" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <input type="hidden" name="_method" value="PUT"/>

                    <div class="form-group">
                        <label for="nome">Nome</label>
                        <input type="text" class="form-control" id="nome" name="nome" value="{{$genero->nome}}" placeholder="Gênero">

                        <br/>
                        <input type="submit" value="Salvar" class="btn btn-primary"/>
                        <input type="reset" value="Limpar" class="btn btn-warning"/>
                        <a href="{{route('generolivro.index')}}" class="btn btn-danger">Cancelar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection