-- MySQL dump 10.13  Distrib 5.5.44, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: social
-- ------------------------------------------------------
-- Server version	5.5.44-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `post_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `comments_post_id_foreign` (`post_id`),
  KEY `comments_user_id_foreign` (`user_id`),
  CONSTRAINT `comments_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatos`
--

DROP TABLE IF EXISTS `contatos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telefone` varchar(255) DEFAULT NULL,
  `mensagem` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `visualizado` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatos`
--

LOCK TABLES `contatos` WRITE;
/*!40000 ALTER TABLE `contatos` DISABLE KEYS */;
INSERT INTO `contatos` VALUES (1,'Aurélio Guedes','aureliomachados@gmail.com','6193226637','Estou tentando enviar uma reclamação','2015-10-09 22:27:44','2015-10-10 01:27:44',1),(2,'teste','tester@gmail.com','teste','sldfj','2015-10-10 01:05:43','2015-10-10 04:05:43',1),(3,'Doe Livros Minas','doelivrosminas@gmail.com','07090808','“O presidente da Câmara nunca recebeu qualquer vantagem de qualquer natureza, de quem quer que seja, referente à Petrobras ou a qualquer outra empresa, órgão publico ou instituição do gênero. Ele refuta com veemência a declaração de que compartilhou qualquer vantagem, com quem quer que seja, e tampouco se utilizou de benefícios para cobrir gasto de qualquer natureza, incluindo pessoal”, disse.\r\n\r\nA Procuradoria-Geral da República informou nesta sexta que há indícios suficientes de que as contas do presidente da Câmara na Suíça não foram declaradas e \"são produto de crime\". Em nota, a PGR afirma que não há mais dúvidas sobre a titularidade das contas em relação a Cunha e a sua mulher, Eduardo Cunha. A PGR pediu ao STF o bloqueio e o sequestro de 2,4 milhões de francos suíços, equivalentes a R$ 9 milhões, atribuídos a Cunha em contas na Suíça.','2015-10-18 20:49:26','2015-10-18 22:49:26',1);
/*!40000 ALTER TABLE `contatos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalhes`
--

DROP TABLE IF EXISTS `detalhes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalhes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quemsomos` text,
  `missao` text,
  `visao` text,
  `objetivos` text,
  `telefone` varchar(255) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `detalhes_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalhes`
--

LOCK TABLES `detalhes` WRITE;
/*!40000 ALTER TABLE `detalhes` DISABLE KEYS */;
INSERT INTO `detalhes` VALUES (1,'Somos..','Nossa missa...','Nossa visao eh..','Nossos objetivos sao...','98745974','teste',6),(2,'fçadfsl fdfdslfjdl sfdsfljdjfljs dfsdfldsjfld jl dfdslfjdljflkdsf dsfsldfjsdlfj dsfld sfldjslfjsd fld fsldfjdslfkj dlkfj dslfjdlsjf dslsfjdlsfkjdsljf dslfldsj fdlsjflkdjlfjdf dsfljdslfkjdsljf ','fçadfsl fdfdslfjdl sfdsfljdjfljs dfsdfldsjfld jl dfdslfjdljflkdsf dsfsldfjsdlfj dsfld sfldjslfjsd fld fsldfjdslfkj dlkfj dslfjdlsjf dslsfjdlsfkjdsljf dslfldsj fdlsjflkdjlfjdf dsfljdslfkjdsljf ','fçadfsl fdfdslfjdl sfdsfljdjfljs dfsdfldsjfld jl dfdslfjdljflkdsf dsfsldfjsdlfj dsfld sfldjslfjsd fld fsldfjdslfkj dlkfj dslfjdlsjf dslsfjdlsfkjdsljf dslfldsj fdlsjflkdjlfjdf dsfljdslfkjdsljf ','asdf\r\nsasdfjdlfj\r\nasljfdljfdljfd\r\nssdljfdsfjdsf\r\n','97495797',NULL,8),(3,'alsdfj','asldfkdfj','alsdfjldsjf','sldfkjdlkfj','93226637','Quadra 11 Conjunto i casa 28 São Sebastião DF',9);
/*!40000 ALTER TABLE `detalhes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doacoes`
--

DROP TABLE IF EXISTS `doacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doacoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `livro` varchar(255) DEFAULT NULL,
  `quantidade` varchar(255) DEFAULT NULL,
  `mensagem` varchar(255) DEFAULT NULL,
  `agradecimento` varchar(255) DEFAULT NULL,
  `validar` binary(1) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `entidade_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doacoes`
--

LOCK TABLES `doacoes` WRITE;
/*!40000 ALTER TABLE `doacoes` DISABLE KEYS */;
INSERT INTO `doacoes` VALUES (2,'A mão e a Luva','1','asfx','Agradecemos pela sua doação.','0',4,8,'2015-10-19 05:03:06','2015-10-19 17:30:45'),(3,'Java Como Programar','2','Bom livro, façam bom proveito desta maravilha','Muito obrigado, a sua doação nos ajudará muito.','0',5,8,'2015-10-19 05:05:15','2015-10-19 17:30:29'),(4,'O Guarani','5','O Guarani, bom livro, espero poder ajudá-los.','Muito obrigado, a sua doação nos ajudará bastante.','0',5,6,'2015-10-19 17:39:24','2015-10-19 17:50:37');
/*!40000 ALTER TABLE `doacoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `friend_user`
--

DROP TABLE IF EXISTS `friend_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `friend_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `friend_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `friend_user_friend_id_foreign` (`friend_id`,`user_id`),
  KEY `fk_friend_user_1_idx` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `friend_user`
--

LOCK TABLES `friend_user` WRITE;
/*!40000 ALTER TABLE `friend_user` DISABLE KEYS */;
INSERT INTO `friend_user` VALUES (76,13,4),(74,4,5),(75,13,5),(78,4,6),(77,13,6);
/*!40000 ALTER TABLE `friend_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `generolivros`
--

DROP TABLE IF EXISTS `generolivros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `generolivros` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `generolivros`
--

LOCK TABLES `generolivros` WRITE;
/*!40000 ALTER TABLE `generolivros` DISABLE KEYS */;
INSERT INTO `generolivros` VALUES (9,'Contos','2015-08-15 22:11:34','2015-08-15 22:11:34'),(10,'Administração','2015-08-15 22:11:42','2015-08-15 22:11:42'),(11,'Literatura','2015-08-15 22:11:47','2015-08-15 22:11:47');
/*!40000 ALTER TABLE `generolivros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `generolivros_users`
--

DROP TABLE IF EXISTS `generolivros_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `generolivros_users` (
  `user_id` int(10) unsigned NOT NULL,
  `generolivros_id` int(10) unsigned NOT NULL,
  KEY `generolivros_users_user_id_foreign` (`user_id`),
  KEY `generolivros_users_generolivros_id_foreign` (`generolivros_id`),
  CONSTRAINT `generolivros_users_generolivros_id_foreign` FOREIGN KEY (`generolivros_id`) REFERENCES `generolivros` (`id`) ON DELETE CASCADE,
  CONSTRAINT `generolivros_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `generolivros_users`
--

LOCK TABLES `generolivros_users` WRITE;
/*!40000 ALTER TABLE `generolivros_users` DISABLE KEYS */;
INSERT INTO `generolivros_users` VALUES (5,9),(5,10),(5,11),(4,9),(4,10),(4,11),(6,10),(6,9),(6,11),(9,9);
/*!40000 ALTER TABLE `generolivros_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_100000_create_password_resets_table',1),('2015_06_13_180424_create_users_table',1),('2015_08_04_231851_create_genero_livros_table',1),('2015_08_16_015038_create_posts_table',2),('2015_08_16_015058_create_comments_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `posts_user_id_foreign` (`user_id`),
  CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (15,'Estou aqui para ajudar à quem mais precisa','Sou uma ong para doação de livros aos inacessíveis.',6,'2015-10-10 20:28:22','2015-10-10 20:28:22'),(16,'A mão e a luva','A mão e a luva é uma história de românce...',9,'2015-10-11 19:47:34','2015-10-11 19:47:34');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `perfil` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_provider_id_unique` (`provider_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (4,'Aurélio Guedes',NULL,'$2y$10$gPrsLd19031sKrdhvLXuauOPjc2Hvf2MVZd6OwvIH.vN2LqImFNsm','aureliomachados@gmail.com','avatar/d7a3f6bbd700d9fec7583d04a3218ce9.png',NULL,NULL,'7eXIfyFpfsJaK1qXTcCPMVrvCDLIHXiJaNpWVdo00i7UOG0ZaxV2LakbjlDU','2015-10-10 01:35:21','2015-10-24 04:35:43','administrador'),(5,'tester',NULL,'$2y$10$IL7S4p4DW6WM0DlgRx2o9OYHapLqfffV8u0GY86inmYjkiYNPly6m','tester@gmail.com','avatar/bd9cc45558903cfa855435d562f098a2.jpg',NULL,NULL,'B27kYqb0KUQaIMsVHCaiMsz4igkMPlngUEVjSU3Lelm4UbtzpNSbwxp3FUxx','2015-10-10 03:43:15','2015-10-19 17:56:53','usuario'),(6,'Doelivros',NULL,'$2y$10$E/DJYrZW9oQrZqPDU9fwOuC4iqseTp.D6HrooLVyPAPX3EravP4a6','doelivrosapp@gmail.com','avatar/b15ea291452d0654ea7efb2378f82bd9.jpg',NULL,NULL,'ObbWJ75bYzOqqlFXfHLIik31q94ghCfJjX5IOqsDLezERtUmvtEONDw6maYV','2015-10-10 20:24:49','2015-10-24 04:46:39','entidade'),(8,'Instituto pró livro',NULL,'$2y$10$mDv3GZJXxKCV1sBeYYjaY.OeRHE8t5ZRbKJrVxN.xH99tQuFmTVsi','institutoprolivro@gmail.com','avatar/f2398c8fdbb45533bbe310b66513aaa3.jpg',NULL,NULL,'vylfjI4k9CAfMybz2p06TbwHTdTTs2jK2sISFrA2IsxWo0ZTq4qg7OLBl07q','2015-10-11 19:41:25','2015-10-19 17:38:32','entidade'),(9,'Instituto Guedes',NULL,'$2y$10$mS0DS6XwW9xNWpRtEEFDAOEmq3Q8oUMzWXRkvIY.8iRdBMo7Y0P2u','institutoguedes@gmail.com','avatar/f052a010f9a52d1589a41209d5a70435.jpg',NULL,NULL,'GDWpZRyNESMnixmvGQ1vEjRdBNNS28akUD1FtQjhrmgiT0igr0LWirJb5n5D','2015-10-11 19:46:13','2015-10-19 14:39:35','entidade');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-26 18:32:14
