<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneroLivrosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('generolivros', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('nome');
			$table->timestamps();
		});

        Schema::create('generolivros_users', function(Blueprint $table){
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('generolivros_id')->unsigned();
            $table->foreign('generolivros_id')->references('id')->on('generolivros')->onDelete('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('generolivros_users');
        Schema::drop('generolivros');
	}

}
