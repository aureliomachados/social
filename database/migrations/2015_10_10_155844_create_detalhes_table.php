<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalhesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('detalhes', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('quemsomos');
            $table->string('missao');
            $table->string('visao');
            $table->string('objetivos');
            $table->string('telefone');
            $table->string('endereco');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('detalhes');
	}

}
