<?php
/**
 * Created by PhpStorm.
 * User: aurelio
 * Date: 05/08/15
 * Time: 15:10
 */

namespace App\Services;


use App\Repositories\UserRepository;
use App\User;
use Illuminate\Support\Facades\Log;

class UserService {

    private $repository;

    public function __construct(UserRepository $repository){
        $this->repository = $repository;
    }

    public function findAll(){
        $users = $this->repository->findAll();

        Log::debug("Lista de usuários carregada");

        return $users;
    }

    public function findAllPaginate(){
        $users = $this->repository->findAllPaginate();

        Log::debug("Lista de usuários carregada com paginação");

        return $users;
    }

    public function findById($id){
        $user = $this->repository->findById($id);

        Log::debug("Usuário carregado");

        return $user;
    }

    public function findByName($name){
        return $this->repository->findByName($name);
    }

    public function delete($id){
        $this->repository->delete($id);

        Log::debug("Usuário removido.");
    }
}