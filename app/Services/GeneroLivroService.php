<?php
/**
 * Created by PhpStorm.
 * User: aurelio
 * Date: 04/08/15
 * Time: 23:11
 */

namespace App\Services;


use App\Http\Requests\GeneroLivroRequest;
use App\Repositories\GeneroLivroRepository;
use Illuminate\Support\Facades\Log;
use App\GeneroLivro;

class GeneroLivroService {

    private $repository;

    public function __construct(GeneroLivroRepository $repository){
        $this->repository = $repository;

        Log::debug("Novo repositorio de genero de livros carregado.");
    }

    public function save(GeneroLivroRequest $request){
        Log::debug("Adicionando genero de livro.");


        return GeneroLivro::create($request->all());
    }

    public function findAll(){

        Log::debug("Buscando todos os gêneros");

        return $this->repository->findAll();
    }

    public function findAllPaginate(){
        Log::debug("Buscando todos os gêneros paginados");

        return $this->repository->findAllPaginate();
    }

    public function findById($id)
    {
        Log::debug("Buscando pelo id: {$id}");

        return $this->repository->findById($id);
    }

    public function update(GeneroLivroRequest $request, $id){
        Log::debug("Atualizando o gênero de id: {$id}");

        return $this->repository->update($request->all(), $id);
    }

    public function delete($id){
        return $this->repository->delete($id);
    }
}