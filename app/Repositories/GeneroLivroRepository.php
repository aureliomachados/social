<?php
/**
 * Created by PhpStorm.
 * User: aurelio
 * Date: 04/08/15
 * Time: 23:01
 */

namespace App\Repositories;


use App\GeneroLivro;
use App\Http\Requests\GeneroLivroRequest;

class GeneroLivroRepository {

    public function save($data){
        return GeneroLivro::create($data);
    }

    public function findAll(){
        return GeneroLivro::all();
    }

    public function findAllPaginate(){
        return GeneroLivro::paginate(3);
    }

    public function findById($id)
    {
        return GeneroLivro::find($id);
    }

    /*
     * Retorna verdadeiro se o usuario for atualizado, e falso caso não.
     */
    public function update($data, $id){

        $genero = null;

        $genero = $this->findById($id);

        if($genero){
            if($genero->update($data, $id)){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

    public function delete($id){

        $genero = null;

        $genero = GeneroLivro::find($id);

        if($genero){
            return $genero->delete($id);
        }
        else{
            return false;
        }
    }
}