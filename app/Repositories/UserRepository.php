<?php
namespace App\Repositories;

use App\User;

class UserRepository {


    public function findAll(){
        return User::all();
    }

    public function findAllPaginate(){
        return User::paginate(3);
    }

    public function findByName($name){
        return User::where("name", 'like', "%" . $name . "%")->get();
    }

    public function findById($id){
        return User::find($id);
    }

    public function delete($id){
        return User::destroy($id);
    }

    //na verdade busca pelo provider_id
    public function findByUserNameOrCreate($userData){

        $user = User::where('provider_id', '=', $userData->id)->first();
        if(!$user) {
            $user = User::create([
                'provider_id' => $userData->id,
                'name' => $userData->name,
                'username' => $userData->nickname,
                'email' => $userData->email,
                'avatar' => $userData->avatar,
                'active' => 1,
            ]);
        }

        //method semantic
        $this->checkIfUserNeedsUpdating($userData, $user);

        //return a user found.
        return $user;
    }

    public function checkIfUserNeedsUpdating($userData, $user){
        $socialData = [
            'avatar' => $userData->avatar,
            'email' => $userData->email,
            'name' => $userData->name,
            'username' => $userData->nickname,
        ];

        $dbData = [
            'avatar' => $user->avatar,
            'email' => $user->email,
            'name' => $user->name,
            'username' => $user->username,
        ];



        if(!empty(array_diff($socialData, $dbData))){
            $user->avatar = $userData->avatar;
            $user->email = $userData->email;
            $user->name = $userData->name;
            $user->username = $userData->nickname;
            $user->save();
        }
    }
}