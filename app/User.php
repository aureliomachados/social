<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'avatar', 'password', 'perfil', 'provider_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

    //usuário tem generos favoritos
    public function generolivros()
    {
        return $this->belongsToMany("App\GeneroLivro", 'generolivros_users', 'user_id', 'generolivros_id');
    }

    //o usuário posssui um ou mais posts
    public function posts(){
        return $this->hasMany('App\Post');
    }

    //o usuário possui muitos amigos.
    public function friends(){
        return $this->belongsToMany('App\User', 'friend_user', 'user_id', 'friend_id');
    }

    //Os amigos que possuem o atual.
    public function theFriends()
    {
        return $this->belongsToMany('App\User', 'friend_user', 'friend_id', 'user_id');
    }

    //Usuário possui detalhe.
    public function detalhe(){
        return $this->hasOne('App\Detalhe');
    }


    public function doacoes()
    {
        return $this->hasMany('App\Doacao', 'user_id');
    }

}
