<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Detalhe extends Model {

	protected $fillable = ['quemsomos', 'visao', 'missao', 'objetivos', 'telefone', 'endereco', 'user_id'];

    public $timestamps = false;

}
