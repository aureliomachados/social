<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//página inicial da aplicação.
Route::get('/', ['as' => 'welcome', 'uses' => 'WelcomeController@index']);


//relacionado aos perfis de usuário.


//rota para o usuário acessar o seu perfil.
Route::get('profile',['as' => 'profile', 'uses' => 'HomeController@profile']);

//rota para que a entidade possa acessar o seu perfil.
Route::get('profile-entity',['as' => 'profile.entity', 'uses' => 'HomeController@profileEntity']);

Route::get('profile/busca', ['as' => 'profile.busca', 'uses' => 'HomeController@busca']);

Route::get('profile/{id}', ['as' => 'profile.details', 'uses' => 'HomeController@details']);

Route::delete('profile/{id}', ['as' => 'profile.destroy', 'uses' => 'HomeController@destroy']);

//pagina home com  a lista de perfis.
Route::get('home', ['as' => 'home', 'uses' => 'HomeController@home']);

//página com a lista de entidades.
Route::get('entidades', ['as' => 'entidades', 'uses' => 'EntidadeController@entidades']);

Route::get('entidades/create', ['as' => 'entidades.create', 'uses' => 'EntidadeController@create']);

Route::get('entidades/{id}', ['as' => 'entidades.details', 'uses' => 'EntidadeController@entidadesDetails']);

Route::post('entidades', ['as' => 'entidades.store', 'uses' => 'EntidadeController@store']);

//rotas relacionadas ao envio de imagem do perfil
Route::get('home/avatar', ['as' => 'avatar.create', 'uses' => 'HomeController@createAvatar']);
Route::post('home/avatar', ['as' => 'avatar.send', 'uses' => 'HomeController@sendAvatar']);

//logicas para seguir usuário...
Route::post('seguir', ['as' => 'seguir', 'uses' => 'HomeController@seguir']);

Route::post('deixar-de-seguir', ['as' => 'deixarDeSeguir', 'uses' => 'HomeController@deixarDeSeguir']);

Route::get('quem-me-segue', function(){
    return Auth::user()->theFriends()->count();
});

//controllers definidos para autenticação e restauração de senha.
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

//serviço de integração com redes sociais.
Route::get('login/{provider?}', 'Auth\AuthController@login');

//resource controllers.
Route::resource("generolivro", 'GeneroLivroController');

Route::resource('posts', 'PostController');


//generos favoritos

Route::get('generos-favoritos/generos', ['as' => 'generosfavoritos.generos', 'uses' => 'GenerosFavoritosController@generos']);

Route::post('generos-favoritos/generos', ['as' => 'generosfavoritos.save', 'uses' => 'GenerosFavoritosController@save']);


//doação
Route::get('doacoes/{id}',['as' => 'doacoes.show', 'uses' => 'DoacaoController@show']);
Route::post('doacoes', ['as' => 'doacoes.send', 'uses' => 'DoacaoController@send']);
Route::put('doacoes', ['as' => 'doacoes.validar', 'uses' => 'DoacaoController@validar']);


//contato
Route::get('contato', ['as' => 'contato', 'uses' => 'ContatoController@contato']);

Route::post('contato', ['as' => 'contato.envia', 'uses' => 'ContatoController@enviaContato']);

Route::get('contato/{id}', ['as' => 'contato.ver', 'uses' => 'ContatoController@ver']);

Route::get('contatos', ['as' => 'contatos', 'uses' => 'ContatoController@listaContatos']);
