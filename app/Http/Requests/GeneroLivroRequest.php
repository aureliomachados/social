<?php namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;

class GeneroLivroRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
        if(Auth::guest() || Auth::user()->perfil == "administrador"){
            return true;
        }
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'nome' => 'required|min:3|max:256|unique:generolivros'
		];
	}

}
