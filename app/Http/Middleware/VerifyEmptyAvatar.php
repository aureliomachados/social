<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

/**
 * Class VerifyEmptyAvatar Verifica se o usuário logado possui foto no perfil, se não, redireciona
 * para a página de adição de imagem.
 * @package App\Http\Middleware
 */
class VerifyEmptyAvatar {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        if(Auth::check()){
            if(Auth::user()->avatar == null){
                return redirect()->route('avatar.create');
            }
        }
		return $next($request);
	}

}
