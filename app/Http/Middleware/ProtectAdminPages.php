<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

/**
 * Class ProtectAdminPages
 * @package App\Http\Middleware
 * Protege as páginas gerenciadas pelo administrador.
 */
class ProtectAdminPages {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        if(Auth::guest() || $request->user()->perfil != "administrador"){
            return redirect()->route('welcome');
        }
		return $next($request);
	}

}
