<?php namespace App\Http\Controllers;

use App\Doacao;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        //busca as doações validadas ordenada pela data de atualização de uma forma decrescente limitando em 10 os registros.
        $doacoes = Doacao::where('validar', '=', 1)->orderBy('updated_at', 'desc')->take(10)->get();

		return view('welcome')->with('doacoes', $doacoes);
	}

}
