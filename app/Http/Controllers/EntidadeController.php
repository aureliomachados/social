<?php namespace App\Http\Controllers;

use App\Detalhe;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Session;
use Laracasts\Flash\Flash;

class EntidadeController extends Controller {

    public function __construct(){
        $this->middleware('auth');
        $this->middleware('admin', ['only' => ['store', 'create']]);
	}

    public function create()
    {
        return view('entidades.create');
    }

    public function store(Request $request){

        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ];

        $this->validate($request, $rules);

        $user = new User();
        $detalhe = new Detalhe();

        $user->name = $request->name;
        $user->email = $request->email;
        $user->perfil = 'entidade';
        $user->password = bcrypt($request->password);

        $detalhe->quemsomos = $request->quemsomos;
        $detalhe->missao = $request->missao;
        $detalhe->visao = $request->visao;
        $detalhe->objetivos = $request->objetivos;
        $detalhe->telefone = $request->telefone;
        $detalhe->endereco = $request->endereco;

        $user->save();

        $userSaved = User::where('email', $request->email)->first();

        $userSaved->detalhe()->save($detalhe);

        Flash::success("Entidade adicionada com sucesso!");

        return redirect()->route('entidades');
    }

    /**
     * Lista todas as entidades.
     */
    public function entidades()
    {
        $entidades = User::where('perfil','=', 'entidade')->paginate();

        return view('entidades.entidades')->with('entidades', $entidades)->with('paginate', true);
    }

    public function entidadesDetails($id){

        $entidade = null;

        if($id == null){
            abort(404);
        }

        //só retorna se for uma entidade.
        $entidade = User::where('perfil', '=', 'entidade')->where('id', '=', $id)->first();

        if($entidade){
            return view('entidades.entidades-details')->with('entidade', $entidade);
        }
        else{
            return redirect()->route('entidades');
        }
    }

}
