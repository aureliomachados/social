<?php namespace App\Http\Controllers;

use App\Contato;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\ContatoRequest;
use Laracasts\Flash\Flash;

class ContatoController extends Controller {

    public function __construct(){

        //define quais ações o visitante poderá acessar.
        $this->middleware('admin', ['except' =>  ['contato', 'enviaContato']]);
    }

    public function contato()
    {
        return view('contato.contato');
	}

    public function enviaContato(ContatoRequest $request){

        $contato =  new Contato();

        $contato->nome = $request->get('nome');
        $contato->email = $request->get('email');
        $contato->telefone = $request->get('telefone');
        $contato->mensagem = $request->get('mensagem');
        $contato->visualizado = 0;

        $contato->save();

        Flash::success("Mensagem recebida com sucesso! Aguarde uma resposta!");

        return redirect()->back();
    }

    public function listaContatos(){
        //retorna a lista de contatos paginados
        $contatos = Contato::all();

        //armazena a quantidade de mensagens não lidas

        $mensagensNaoLidas = Contato::where('visualizado', '=', 0)->count();

        //(obs)a quantidade de mensagens lidas se dará pela subtração com a quantidade de mensagens não lidas.


        return view('contato.lista-contatos')->with('contatos', $contatos)->with('mensagensNaoLidas', $mensagensNaoLidas);
    }

    public function ver($id){

        $contato = Contato::find($id);

        if($contato->visualizado == 0){
            $contato->visualizado = 1;

            $contato->save();
        }

        return view('contato.ver')->with('contato', $contato);
    }

}
