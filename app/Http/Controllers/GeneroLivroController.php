<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Services\GeneroLivroService;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class GeneroLivroController extends Controller {

    const MENSSAGEM_REMOVIDO = "O gênero pode ter sido removido!";

    private $service;

    public function __construct(GeneroLivroService $service){
        $this->service = $service;

        //registra o middleware de controle de acesso para os recursos do controller.
        $this->middleware('admin');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$generos = $this->service->findAllPaginate();

        return view('generolivro.index')->with('generos', $generos);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('generolivro.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Requests\GeneroLivroRequest $request)
	{
		$this->service->save($request);

        Flash::success('Gênero adicionado!');

        return redirect()->route('generolivro.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        if($id == null){
            return abort(404);
        }
        else{

            $genero = null;

            $genero = $this->service->findById($id);

            if($genero){
                return view('generolivro.show')->with('genero', $genero);
            }
            else{
                Flash::warning(self::MENSSAGEM_REMOVIDO);

                return redirect()->route("generolivro.index");
            }

        }
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        if($id == null){
            abort(404);
        }else{
            $genero = null;

            $genero = $this->service->findById($id);

            if($genero){
                return view('generolivro.edit')->with('genero', $genero);
            }
            else{
                Flash::warning(self::MENSSAGEM_REMOVIDO);
                return redirect()->route('generolivro.index');
            }

        }

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Requests\GeneroLivroRequest $request, $id)
	{
        if($id == null){
            abort(404);
        }else
        {
            $genero = $this->service->update($request, $id);

            if($genero){

                Flash::warning("Gênero atualizado!");

                return redirect()->route('generolivro.index');
            }
            else{
                Flash::warning("Não foi possível atualizar. " . self::MENSSAGEM_REMOVIDO);
                return redirect()->route('generolivro.index');
            }
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        if($id == null){
            abort(404);
        }
        else{
            if($this->service->delete($id)){
                Flash::success("Gênero removido!");

                return redirect()->route('generolivro.index');
            }
            else{
                Flash::warning(self::MENSSAGEM_REMOVIDO);
                return redirect()->route('generolivro.index');
            }
        }
	}

}
