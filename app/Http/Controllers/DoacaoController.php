<?php namespace App\Http\Controllers;

use App\Doacao;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\DoacaoRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Laracasts\Flash\Flash;

class DoacaoController extends Controller {

    private $entidade;
    private $usuario;

    public function __construct(){
        $this->middleware('auth');
    }


    public function send(DoacaoRequest $request)
    {
        $doacao = $request->all();

        $doacao['validar'] = false;

        Doacao::create($doacao);

        //recupera o atores que participam da doação.
        $this->entidade = User::find($doacao['entidade_id']);

        $this->usuario = User::find($doacao['user_id']);

        //compacta os dados para serem disponibilizadas na view enviadoacao.blade.php
        $data = ['entidade' => $this->entidade, 'usuario' => $this->usuario];

        Mail::send('emails.enviadoacao', $data, function($message){
            $message->from('doelivrosapp@gmail.com', 'Doe Livros');
            $message->to($this->entidade->email);
            $message->subject("Nova doação");
        });

        Log::info("Doação realizada pelo usuário de id {$request->user_id} para a entidade de id {$request->entidade_id}");

        Flash::success("Doação formalizada com sucesso. Aguarde a aprovação da entidade para confirmar a chegada do(s) livro(s) ");

        return redirect()->back();
    }

    public function show($id){
        if($id == null){
            abort(404);
        }

        $doacao = Doacao::find($id);

        $doador = User::where('id', $doacao->user_id)->first();

        return view('doacao.show')->with('doacao', $doacao)->with('user', Auth::user())->with('doador', $doador);
    }

    public function validar(Request $request)
    {
        $doacao = Doacao::find($request->id);

        $doacao->validar = true;

        $doacao->agradecimento = $request->agradecimento;

        $doacao->save();


        //recupera o atores que participam da doação.
        $this->entidade = User::find($doacao->entidade_id);

        $this->usuario = User::find($doacao->user_id);

        //compacta os dados para serem disponibilizadas na view enviadoacao.blade.php
        $data = ['entidade' => $this->entidade, 'usuario' => $this->usuario, 'agradecimento' => $request->agradecimento];

        Mail::send('emails.validadoacao', $data, function($message){
            $message->from('doelivrosapp@gmail.com', 'Doe Livros');
            $message->to($this->usuario->email);
            $message->subject("Doação recebida");
        });


        Flash::success("Doação validada com sucesso!");

        return redirect()->route('profile.entity');
    }
}
