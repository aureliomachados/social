<?php namespace App\Http\Controllers;

use App\Doacao;
use App\Services\UserService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\Facades\Image;
use Laracasts\Flash\Flash;

class HomeController extends Controller {

    private $service;
	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(UserService $service)
	{
        $this->service = $service;

		$this->middleware('auth');
        $this->middleware('avatar', ['except' => ['createAvatar', 'sendAvatar']]);
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function home()
	{
        $users = $this->service->findAllPaginate();

        $generos = $this->meusGeneros();

		return view('home')->with('users', $users)->with('paginate', true)->with('generos', $generos);
	}

    //busca pelo nome.
    public function busca(Request $request){
        $users = null;
        if($request->has('name')){
            $users = $this->service->findByName($request->get('name'));
        }

        $generos = User::find(Auth::user()->id)->generolivros;

        return view('home')->with('users', $users)->with('paginate', false)->with('generos', $generos);
    }

    public function profile(){

        $generos = $this->meusGeneros();

        $user = $this->getUser();

        return view('profile')->with('generos', $generos)->with('user', $user);

    }

    /*
     * Acessa as informações do perfil da entidade.
     */
    public function profileEntity()
    {
        $user = $this->getUser();

        if($user->perfil != "entidade"){
            //perfil adequado para usuários.
            return redirect()->route('profile');
        }

        $generos = $this->meusGeneros();

        $doacoes = Doacao::where('entidade_id', $user->id)->orderBy('id', 'desc')->get();

        return view('profile-entity')->with('generos', $generos)->with('user', $user)->with('doacoes', $doacoes);
    }

    public function details($id){

        if($id == null){
            abort(404);
        }

        $user = $this->service->findById($id);

        return view('details')->with('user', $user);
    }

    /**retorna os gêneros do usuário logado
     * @return mixed
     */
    protected function meusGeneros(){
        return User::find(Auth::user()->id)->generolivros;
    }


    /**
     * retorna todas as informações do usuário logado.
     * @return \Illuminate\Support\Collection|null|static
     */
    protected function getUser()
    {
        return User::find(Auth::user()->id);
    }
    
    //seguir
    public function seguir(Request $request){
        $user_id = $request->get('user_id');

        Auth::user()->friends()->attach($user_id);

        $user = $this->service->findById($user_id);


        Flash::message("Você começou a seguir {$user->name}");

        return redirect()->back();
    }

    public function deixarDeSeguir(Request $request){
        $user_id = $request->get('user_id');

        Auth::user()->friends()->detach($user_id);

        $user = $this->service->findById($user_id);

        Flash::message("Você deixou de seguir {$user->name}");

        return redirect()->back();
    }

    /*
     * Aqui está as ações para adição de avatar.
     */

    public function createAvatar()
    {
        return view('avatar.create');
    }

    public function sendAvatar(Request $request){

        //verifica se o arquivo é uma imagem.
        $this->validate($request, ['avatar' => 'mimes:jpeg,png,jpg']);

        if($request->hasFile('avatar')) {
            $path =  "avatar/";

                //verifica se o arquivo é válido.
            if ($request->file('avatar')->isValid()) {

                //concatena um hash da hora atual com a extensão do arquivo.
                $avatar = md5(time()) . '.' . $request->file('avatar')->getClientOriginalExtension();

                //make
                $image = Image::make($request->file('avatar'));

                //resize
                $image->resize(160, 160);

                //concatena o caminho com o nome gerado para a imagem.
                $realPath = $path . $avatar;

                //pega o usuário
                $user = User::find($request->user()->id);

                //verifica se não existe imagem no perfil.
                if($user->avatar == null) {
                    $user->avatar = $realPath;
                }
                else{
                    //remove a imagem antiga.
                    unlink($user->avatar);
                    $user->avatar = $realPath;
                }

                $user->save();

                Flash::success("Imagem adicionada com sucesso!");

                //move to directory
                $image->save($path . $avatar);

                return redirect()->route('profile');
            }
        }
        else{
            Flash::warning('Selecione uma imagem!');
            return redirect()->back();
        }
    }


    /**
     * Remove o usuário do aplicativo.
     * @param $id
     */
    public function destroy($id){
        if($id == null){
            abort(404);
        }
        $user = User::find($id);

        if($user->destroy($user->id)){
            unlink($user->avatar);
        }

        Flash::success("Conta excluida!");

        return redirect()->to('/auth/login');
    }


}
