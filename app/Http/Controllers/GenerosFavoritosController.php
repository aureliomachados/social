<?php namespace App\Http\Controllers;

use App\GeneroLivro;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Services\GeneroLivroService;
use App\Services\UserService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;

class GenerosFavoritosController extends Controller {

    private $userService;
    private $generoService;

    public function __construct(UserService $userService, GeneroLivroService $generoLivroService){

        $this->userService = $userService;
        $this->generoService = $generoLivroService;

        $this->middleware('auth');
	}

    public function generos(){


        $user = User::find(Auth::user()->id);

        //retorna os gêneros disponiveis para o usuário adicionar aos seus favoritos.
        $generosDisponiveis = $this->generosDisponiveis();

        //generos necessarios para recarregar a lista do usuario
        $meusGeneros = $user->generolivros;

        return view('generos-favoritos.generos')->with('generos', $generosDisponiveis)->with('meusGeneros', $meusGeneros);
    }

    public function save(Request $request){

        //id do gênero
        $generoId = $request->get('genero_id');

        $user = $this->userService->findById(Auth::user()->id);

        $user->generolivros()->attach($generoId);

        Flash::message("Genero adicionado aos favoritos");

        return redirect()->route('generosfavoritos.generos');
    }


    /**
     * lista os generos que o usuario ainda nao possui nos seus favoritos.
     * @return mixed
     */
    protected function generosDisponiveis(){

        //busca o usuário logado.
        $user = User::find(Auth::user()->id);

        //carrega os gêneros favoritos do usuário
        $generos = $user->generolivros;

        //irá armazenar o id dos gêneros do usuário.
        $idMeusGeneros = array();

        //armazena os ids
        foreach($generos as $g){
            $idMeusGeneros[$g->id] = $g->id;
        }

        //retorna os gêneros que ainda não estejam na lista de gêneros do usuário.
        return $generosDisponiveis = DB::table('generolivros')->whereNotIn('id', $idMeusGeneros)->get();

    }

}
