<?php
namespace App;

use Illuminate\Contracts\Auth\Guard;
use Laravel\Socialite\Contracts\Factory as Socialite;
use App\Repositories\UserRepository;use App\Http\Requests\Request;

class AuthenticateUser {

    private $socialite;
    private $auth;
    private $users;

    public function __construct(Socialite $socialite, Guard $auth, UserRepository $users){
        $this->socialite = $socialite;
        $this->auth = $auth;
        $this->users = $users;
    }

    //executa a lógica de negócio relacionada ao login com rede social.
    public function execute($request, $listener, $provider){
        if(!$request) return $this->getAuthorizationFirst($provider);
            $user = $this->users->findByUserNameOrCreate($this->getSocialUser($provider));

        $this->auth->login($user, true);

        return $listener->userHasLoggedIn($user);
    }

    //requer primeiramente a autorização
    private function getAuthorizationFirst($provider){
        return $this->socialite->driver($provider)->redirect();
    }

    //retorna o usuário da rede social pelo seu provedor (facebook, google...)
    private function getSocialUser($provider)
    {
        return $this->socialite->driver($provider)->user();
    }
}