<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneroLivro extends Model {

    protected $table = 'generolivros';

    protected $fillable = ['nome'];

    public function users()
    {
        return $this->belongsToMany('App\User', 'generolivros_users', 'user_id', 'generolivros_id');
    }
}
