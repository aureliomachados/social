<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Doacao extends Model {

    protected $table = 'doacoes';

    protected $fillable = ['livro', 'quantidade', 'mensagem', 'agradecimento', 'validar', 'user_id', 'entidade_id'];


    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function entidade()
    {
        return $this->belongsTo('App\User', 'entidade_id');
    }

}
